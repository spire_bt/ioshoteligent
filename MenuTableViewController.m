//
//  MenuTableViewController.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/5/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "MenuTableViewController.h"
#import "SWRevealViewController.h"
#import "ViewController.h"
#import "UsersWork.h"
#import "HotelsWork.h"

@interface MenuTableViewController ()


@end

@implementation MenuTableViewController{
    NSMutableArray *menuElements;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    
    menuElements = [NSMutableArray arrayWithObjects: @"Housekeeping", @"Maintenance", @"Notifications", @"Feedback", @"Logout", nil];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [menuElements count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = [menuElements objectAtIndex: indexPath.row];
    //UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier forIndexPath:indexPath];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    // Configure the cell...
    cell.textLabel.text = [menuElements objectAtIndex:indexPath.row];
    
    return cell;
}
/*-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    
}*/
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 3) {
        NSLog(@"Feedback");
    }
    if (indexPath.row == 4) {

        UIStoryboard* storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        ViewController *add = [storyboard instantiateViewControllerWithIdentifier:@"viewController"];
        [self presentViewController:add animated:YES completion:nil];
    }
}
- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UsersWork *usersWork = [[UsersWork alloc]init];
    HotelsWork *hotelWork = [[HotelsWork alloc] init];
    
    NSString *firstName = [usersWork getFirstName];
    NSString *lastName = [usersWork getLastName];
    NSString *hotelName = [hotelWork getName];

    NSString *userName = [usersWork getUsername];
    
    NSString *infoString = [NSString stringWithFormat:@"%@ %@\n%@\n%@",firstName,lastName,hotelName,userName];
    CGRect screenRect = [[UIScreen mainScreen] bounds];

    UIView *headerView = [[UIView alloc] init];
    UILabel *headerLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, screenRect.size.width*0.8, screenRect.size.height * 0.25)];
    headerLabel.font = [headerLabel.font fontWithSize:14];
    headerLabel.numberOfLines = 4;
    headerLabel.lineBreakMode = NSLineBreakByWordWrapping;
    [headerLabel setTextAlignment:NSTextAlignmentRight];
    [headerLabel setText:infoString];
    [headerLabel setTextColor:[UIColor whiteColor]];
    [headerView addSubview:headerLabel];
    [headerView setBackgroundColor:[UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0]];
     
    return headerView;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.height * 0.25;
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ( [segue isKindOfClass: [SWRevealViewControllerSegue class]] ) {
        SWRevealViewControllerSegue *swSegue = (SWRevealViewControllerSegue*) segue;
        
        swSegue.performBlock = ^(SWRevealViewControllerSegue* rvc_segue, UIViewController* svc, UIViewController* dvc) {
            
            UINavigationController* navController = (UINavigationController*)self.revealViewController.frontViewController;
            [navController setViewControllers: @[dvc] animated: NO ];
            [self.revealViewController setFrontViewPosition: FrontViewPositionLeft animated: YES];
        };
        
    }
    
}


@end
