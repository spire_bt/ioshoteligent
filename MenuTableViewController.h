//
//  MenuTableViewController.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/5/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuTableViewController : UITableViewController
@property (strong, nonatomic) IBOutlet UITableView *menuTableView;


@end
