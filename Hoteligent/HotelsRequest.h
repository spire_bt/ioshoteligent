//
//  Hotels.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HotelsDelegate <NSObject>
-(void)getHotelsSuccesWithResponse:(id)response;
-(void)getHotelsError:(NSError *)error;
@end


@interface HotelsRequest : NSObject{
    __weak id <HotelsDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)getHotelsIdsForToken:(NSString *)token;

@end
