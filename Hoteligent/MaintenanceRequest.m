//
//  Maintenance.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/13/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "MaintenanceRequest.h"
#import "RequestHelper.h"


@implementation MaintenanceRequest

- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}


-(void)getMaintenance:(NSString *)attributes withToken:(NSString *)token{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    NSString *urlString = [NSString stringWithFormat:@"api/maintenance/%@",attributes];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper getRequestWithParameters:nil forUrl:urlString andHeader:dic];
    
}
-(void)putMaintenance:(NSMutableDictionary *)jsonDictionary withToken:(NSString *)token forHotelId:(NSString*)hotelId andMaintenanceActivity:(NSString *)activityId{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    NSString *urlString = [NSString stringWithFormat:@"api/maintenance/%@/%@",hotelId,activityId];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper putRequestWithParameters:jsonDictionary forUrl:urlString andHeader:dic];
}

-(void)didReceiveResponse:(id)response{
    [_delegate getMaintenanceSuccesWithResponse:response];
    
}
-(void)didReceiveError:(NSError *)error{
    [_delegate getMaintenanceError:error];
    
}

@end
