//
//  Token.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Token : NSManagedObject

@property (nonatomic, retain) NSString * access_token;
@property (nonatomic, retain) NSNumber * expires_in;
@property (nonatomic, retain) NSString * for_username;
@property (nonatomic, retain) NSDate * issued;

@end
