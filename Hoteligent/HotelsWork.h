//
//  HotelsWork.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/4/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Hotels.h"
//#import "User.h"

@interface HotelsWork : NSObject{
    
}
@property(nonatomic, strong) AppDelegate *appDelegate;
-(void)setHotelsTable:(NSMutableDictionary *)hotelsDictionary;
-(NSString *)getId;
-(NSString *)getName;
//-(NSArray *)getUsers;
-(NSString *)getAddress;
-(NSString *)getCity;
-(NSString *)getPhone;
@end
