//
//  ViewController.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/5/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "ViewController.h"
#import "LogInRequest.h"
#import "HotelsRequest.h"
#import "HotelFromIdRequest.h"
#import "TokenWork.h"
#import "HotelsWork.h"
#import "UsersWork.h"

@interface ViewController ()


@end

@implementation ViewController
static dispatch_once_t onceToken;
static dispatch_once_t onceHotelsForToken;
static dispatch_once_t onceHotelByIds;

- (void)viewDidLoad {
    [super viewDidLoad];
    /*
     Roles
     Admin 0
     GeneralManager 1
     FrontDesk 2
     HeadHousekeeping 3
     Housekeeping 4
     Maintenance 5
     */
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    [_username setDelegate:self];
    [_password setDelegate:self];


}


-(IBAction)login:(id)sender{
    if ([_username.text length]<5 && [_username.text rangeOfString:@"@"].location != NSNotFound) {
        /*TODO: Error Alert*/
       
    }else if([_password.text length]<4){
        /*TODO: Error Alert*/
        
    }else{
        NSDictionary *dic = [NSDictionary dictionaryWithObjectsAndKeys:_username.text, @"username", _password.text, @"password", @"password", @"grant_type", nil];
        LogInRequest *login = [[LogInRequest alloc] init];
        [login setDelegate:self];
        
        dispatch_once (&onceToken, ^{
            [login doLoginWithAttributes:dic];
        });
    }
    
}

-(void)loginSuccesWithResponse:(id)response{
    /*TODO: Add this to Data Base*/

    TokenWork *tokenWork = [[TokenWork alloc] init];
    [tokenWork setTokenTable:response];
    _usernameString =[tokenWork getUsernameForToken];
    _accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];
    HotelsRequest *hotels = [[HotelsRequest alloc] init];
    [hotels setDelegate:self];
    dispatch_once (&onceHotelsForToken, ^{
        [hotels getHotelsIdsForToken:_accessToken];
    });

}
-(void)loginError:(NSError *)error{
    /*TODO: Error Alert*/
    [self gotErrorForRequest:error];


}
-(void)getHotelsSuccesWithResponse:(id)response{
    for (int i = 0; i< [response count]; i++) {
        NSMutableArray *usersArray = [NSMutableArray arrayWithArray:[[response objectAtIndex:i] valueForKey:@"users"]];
        for (int j = 0; j<[usersArray count]; j++) {
            NSString *username = [NSString stringWithFormat:@"%@",[[usersArray objectAtIndex:j] valueForKey:@"userName"]];
            if ([username isEqualToString:_usernameString]) {
                /*TODO: Add this to Data Base*/
                _hotelId = [[response objectAtIndex:i] valueForKey:@"id"];
                _firstName = [[usersArray objectAtIndex:j] valueForKey:@"firstName"];
                _lastName = [[usersArray objectAtIndex:j] valueForKey:@"lastName"];
                _userRole = [[usersArray objectAtIndex:j] valueForKey:@"roles"];
                HotelFromIdRequest *hotelFromId = [[HotelFromIdRequest alloc]init];
                [hotelFromId setDelegate:self];
                dispatch_once (&onceHotelByIds, ^{
                    [hotelFromId getHotelByIds:_hotelId withToken:_accessToken];
                });
            }else if ([_usernameString isEqualToString:@"admin@test.com"]){
                HotelsWork *hotelsWork = [[HotelsWork alloc] init];
                [hotelsWork setHotelsTable:[response objectAtIndex:1]];

                _hotelId = [[response objectAtIndex:1] valueForKey:@"id"];
                _userRole = [NSArray arrayWithObject:0];

                HotelFromIdRequest *hotelFromId = [[HotelFromIdRequest alloc]init];
                [hotelFromId setDelegate:self];
                dispatch_once (&onceHotelByIds, ^{
                    [hotelFromId getHotelByIds:_hotelId withToken:_accessToken];
                });

            }else{
                //  NSLog(@"No Hotel For Username");

            }
        }
        
    }

    
}
-(void)getHotelsError:(NSError *)error{
    /*TODO: Error Alert*/
    [self gotErrorForRequest:error];


}
-(void)getHotelByIdSuccesWithResponse:(id)response{
    /*TODO: Add this to Data Base*/
    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    [hotelsWork setHotelsTable:response];

    _hotelName = [hotelsWork getName];
    _hotelPhone = [hotelsWork getPhone];
    _hotelAddress = [hotelsWork getAddress];
    _hotelCity = [hotelsWork getCity];
    
    
    NSMutableDictionary *userInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:_usernameString, @"userName", _firstName!=nil?_firstName:@"", @"firstName", _lastName!=nil?_firstName:@"", @"lastName", _hotelId, @"hotelId", _userRole, @"roles", nil];

    UsersWork *usersWork = [[UsersWork alloc]init];
    [usersWork setUserTable:userInfo];
    
    onceToken = 0;
    onceHotelsForToken = 0;
    onceHotelByIds = 0;
    
    [self performSegueWithIdentifier:@"Login" sender:self];

    
}
-(void)getHotelByIdError:(NSError *)error{
    /*TODO: Error Alert*/
    [self gotErrorForRequest:error];

    
}
-(void)gotErrorForRequest:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // optional - add more buttons:
    [alert show];

    onceToken = 0;
    onceHotelsForToken = 0;
    onceHotelByIds = 0;

}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


-(IBAction)resignKeyboard:(id)sender{
    [self.view endEditing:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (void)viewWillAppear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    //CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -100;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
