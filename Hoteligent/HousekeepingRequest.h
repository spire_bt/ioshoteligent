//
//  Housekeeping.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/13/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HousekeepingDelegate <NSObject>
-(void)getHousekeepengSuccesWithResponse:(id)response;
-(void)getHousekeepengError:(NSError *)error;
@end


@interface HousekeepingRequest : NSObject{
    __weak id <HousekeepingDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)getHousekeepeng:(NSString *)attributes withToken:(NSString *)token;
-(void)putHousekeepeng:(NSMutableDictionary *)jsonDictionary withToken:(NSString *)token forHotelId:(NSString*)hotelId andHousekeepingActivity:(NSString *)activityId;
@end
