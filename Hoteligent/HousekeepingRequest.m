//
//  Housekeeping.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/13/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "HousekeepingRequest.h"
#import "RequestHelper.h"


@implementation HousekeepingRequest
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}


-(void)getHousekeepeng:(NSString *)attributes withToken:(NSString *)token{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    NSString *urlString = [NSString stringWithFormat:@"api/housekeeping/%@",attributes];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper getRequestWithParameters:nil forUrl:urlString andHeader:dic];
    
}
-(void)didReceiveResponse:(id)response{
    [_delegate getHousekeepengSuccesWithResponse:response];
    
}
-(void)didReceiveError:(NSError *)error{
    [_delegate getHousekeepengError:error];
    
}
-(void)putHousekeepeng:(NSMutableDictionary *)jsonDictionary withToken:(NSString *)token forHotelId:(NSString*)hotelId andHousekeepingActivity:(NSString *)activityId{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    NSString *urlString = [NSString stringWithFormat:@"api/housekeeping/%@/%@",hotelId,activityId];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper putRequestWithParameters:jsonDictionary forUrl:urlString andHeader:dic];
}
@end
