//
//  Rooms.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "RoomsRequest.h"
#import "RequestHelper.h"


@implementation RoomsRequest
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}
-(void)getRoomsIdsForToken:(NSString *)token{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper getRequestWithParameters:nil forUrl:@"api/rooms" andHeader:dic];
    
}
-(void)didReceiveResponse:(id)response{
    [_delegate getRoomsSuccesWithResponse:response];
    
}
-(void)didReceiveError:(NSError *)error{
    [_delegate getRoomsError:error];
    
}

@end
