//
//  HousekeepengDetailsViewController.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/28/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>
typedef enum
{
    Pending = 0,
    Passed = 1,
    Failed = 2,
} InspectionStatus;
typedef enum
{
    passButtonTag = 1,
    failButtonTag = 2,
} inspectionButtonsTag;


@interface HousekeepengDetailsViewController : UIViewController<UITextViewDelegate>
@property (nonatomic, strong) NSMutableDictionary *contentArray;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIScrollView *housekeepengScrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) NSTimer *myTimer;
@property (strong, nonatomic) NSTimer *pauseTimer;
@property (strong, nonatomic) UILabel *createdLabel;
@property (strong, nonatomic) UIButton *startPauseButton;
@property (strong, nonatomic) UIView *timerView;
@property (strong, nonatomic) UIView *inspectionView;
@property (strong, nonatomic) UIView *notesView;
@property (strong, nonatomic) UIView *allCategoriesView;
@property (strong, nonatomic) UIView *completeView;
@property (strong, nonatomic) UITextView *notesTextView;
@property (strong, nonatomic) NSString *pauseValue;
@property (strong, nonatomic) NSDate *startDate;

@property CGSize screenSize;
@property CGFloat notesOrigin;
@property int currentTimeInSeconds;
@property int pausedTimeInSeconds;
@property int statusForActivity;
@end
