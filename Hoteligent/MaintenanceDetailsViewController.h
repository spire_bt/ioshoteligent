//
//  MaintenanceDetailsViewController.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/28/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaintenanceDetailsViewController : UIViewController<UITextViewDelegate>
@property (nonatomic, strong) NSMutableArray *contentArray;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIScrollView *maintenanceScrollView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (strong, nonatomic) UIView *statusView;
@property (strong, nonatomic) UIView *notesView;
@property (strong, nonatomic) UIView *itemsView;
@property (strong, nonatomic) UIView *completeView;
@property (strong, nonatomic) UITextView *notesTextView;
@property (strong, nonatomic) UILabel *createdLabel;
@end
