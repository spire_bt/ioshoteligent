//
//  CustomDictionary.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/3/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CustomDictionary : NSMutableDictionary

@end
