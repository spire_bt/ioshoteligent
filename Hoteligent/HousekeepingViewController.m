//
//  HousekeepingViewController.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/18/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "HousekeepingViewController.h"
#import "SWRevealViewController.h"
#import "HousekeepingRequest.h"
#import "HousekeepengDetailsViewController.h"
#import "HotelsWork.h"
#import "TokenWork.h"

@interface HousekeepingViewController ()


@end

@implementation HousekeepingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setTitle:@"Housekeeping"];
    // Do any additional setup after loading the view.

}

- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    TokenWork *tokenWork = [[TokenWork alloc] init];
    NSString *accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);

    dispatch_async(queue, ^{
        HousekeepingRequest *housekeepeng = [[HousekeepingRequest alloc] init];
        [housekeepeng setDelegate:self];
        [housekeepeng getHousekeepeng:[hotelsWork getId] withToken:accessToken];
        
    });
    
}

-(void)getHousekeepengSuccesWithResponse:(id)response{
    
    if ([response count]!=0) {
        _housekeepArray = [NSMutableArray arrayWithArray:response];
        [_housekeepengTableView reloadData];

    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Empty Housekeepeng Response" message:@"Nothing is assigned to you, or the response can not be parsed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // optional - add more buttons:
        [alert show];

    }
}
-(void)getHousekeepengError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // optional - add more buttons:
    [alert show];

}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_housekeepArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    NSString *status = [NSString string];
    switch ([[[_housekeepArray objectAtIndex:indexPath.row] valueForKey:@"status"]intValue]) {
        case 0:
            status = @"CREATED";
            break;
        case 1:
            status = @"STARTED";
            break;
        case 2:
            status = @"PAUSED";
            break;
        case 3:
            status = @"INSPECTION";
            break;
        case 4:
            status = @"COMPLETED";
            break;
        default:
            status = @"";
            break;
    }

    // Configure the cell...
    NSString *cellInfo = [NSString stringWithFormat:@"Room No. %@ - %@",[[_housekeepArray objectAtIndex:indexPath.row] valueForKey:@"roomNumber"],[[_housekeepArray objectAtIndex:indexPath.row] valueForKey:@"roomType"]];
    cell.textLabel.font = [cell.textLabel.font fontWithSize:10];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text = cellInfo;
    
    cell.detailTextLabel.font = [cell.detailTextLabel.font fontWithSize:8];
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.detailTextLabel.text =[NSString stringWithFormat:@"Created: %@ \nStatus: %@ \nAssigned to: %@",[[_housekeepArray objectAtIndex:indexPath.row] valueForKey:@"created"],status,[[_housekeepArray objectAtIndex:indexPath.row] valueForKey:@"assignedTo"]];
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.height*0.2;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //[self performSegueWithIdentifier:@"DetailedHousekeeping" sender:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *path = [_housekeepengTableView indexPathForSelectedRow];
    HousekeepengDetailsViewController *detailsController =[segue destinationViewController];
    detailsController.contentArray = [_housekeepArray objectAtIndex:path.row];
    detailsController.statusForActivity =[[[_housekeepArray objectAtIndex:path.row] valueForKey:@"status"]intValue];
}


@end
