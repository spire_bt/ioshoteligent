//
//  NotificationsRequest.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/13/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol NotificationsDelegate <NSObject>
-(void)getNotificationsSuccesWithResponse:(id)response;
-(void)getNotificationsError:(NSError *)error;
@end


@interface NotificationsRequest : NSObject{
    __weak id <NotificationsDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)getNotifications:(NSString *)attributes withToken:(NSString *)token;

@end
