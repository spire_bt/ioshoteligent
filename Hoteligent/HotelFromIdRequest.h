//
//  HotelFromId.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HotelFromIdDelegate <NSObject>
-(void)getHotelByIdSuccesWithResponse:(id)response;
-(void)getHotelByIdError:(NSError *)error;
@end


@interface HotelFromIdRequest : NSObject{
    __weak id <HotelFromIdDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)getHotelByIds:(NSString *)attributes withToken:(NSString *)token;

@end
