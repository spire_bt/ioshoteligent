//
//  CustomLabel.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/30/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomLabel : UILabel
@property (nonatomic, assign) UIEdgeInsets edgeInsets;
@end
