//
//  TokenWork.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/4/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "TokenWork.h"

@implementation TokenWork

- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    _appDelegate=(AppDelegate* )[UIApplication sharedApplication].delegate;
    return self;
}
-(void)setTokenTable:(NSMutableDictionary *)tokenDictionary{

    NSString *dateString = [tokenDictionary valueForKey:@".issued"];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"EE, d LLLL yyyy HH:mm:ss Z"];
    NSDate *date = [dateFormat dateFromString:dateString];
    
    Token *tokenObject = [NSEntityDescription
                                      insertNewObjectForEntityForName:@"Token"
                                      inManagedObjectContext:_appDelegate.managedObjectContext];
    
    tokenObject.access_token = [tokenDictionary valueForKey:@"access_token"];
    tokenObject.expires_in = [tokenDictionary valueForKey:@"expires_in"];
    tokenObject.for_username = [tokenDictionary valueForKey:@"userName"];
    tokenObject.issued = date;
    
    
    NSError *error;
    if (![_appDelegate.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        
    }else{
        

    }
}
-(NSString *)getToken{
    NSString *tokenString;

    for (Token *info in [self fetchObjects]) {
        tokenString = info.access_token;
    }
    return tokenString;

}
-(NSDate *)getIssued{
    NSDate *issuedDate;
    
    for (Token *info in [self fetchObjects]) {
        issuedDate = info.issued;
    }
    return issuedDate;
}
-(NSString *)getUsernameForToken{
    NSString *for_usernameString;
    
    for (Token *info in [self fetchObjects]) {
        for_usernameString = info.for_username;
    }
    return for_usernameString;
}
-(NSNumber *)getExpiresIn{
    NSNumber *expires_inString;
    
    for (Token *info in [self fetchObjects]) {
        expires_inString = info.expires_in;
    }
    return expires_inString;
}

-(NSArray *)fetchObjects{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Token" inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];

    return fetchedObjects;
}
@end
