//
//  HotelFromId.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "HotelFromIdRequest.h"
#import "RequestHelper.h"


@implementation HotelFromIdRequest
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}


-(void)getHotelByIds:(NSString *)attributes withToken:(NSString *)token{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    NSString *urlString = [NSString stringWithFormat:@"api/hotels/%@",attributes];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper getRequestWithParameters:nil forUrl:urlString andHeader:dic];
    
}
-(void)didReceiveResponse:(id)response{
    [_delegate getHotelByIdSuccesWithResponse:response];
    
}
-(void)didReceiveError:(NSError *)error{
    [_delegate getHotelByIdError:error];
    
}

@end
