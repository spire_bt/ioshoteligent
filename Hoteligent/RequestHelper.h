//
//  RequestHelper.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/6/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol RequestHelperDelegate <NSObject>
-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
@end


@interface RequestHelper : NSObject{
    id <RequestHelperDelegate> _delegate;
}
@property (nonatomic, strong)  id delegate;
-(void)postRequestWithParameters:(NSDictionary *)parameters forUrl:(NSString *)url;
-(void)getRequestWithParameters:(NSDictionary *)parameters forUrl:(NSString *)url andHeader:(NSDictionary *)header;
-(void)putRequestWithParameters:(NSMutableDictionary *)jsonDictionary forUrl:(NSString *)url andHeader:(NSDictionary *)header;
@end
