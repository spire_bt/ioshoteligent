//
//  NotificationDetailsViewController.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/18/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NotificationDetailsViewController : UIViewController<UITextViewDelegate>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backButton;


@end
