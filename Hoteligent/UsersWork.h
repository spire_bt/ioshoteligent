//
//  UsersWork.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/4/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "User.h"

@interface UsersWork : NSObject
@property(nonatomic, strong) AppDelegate *appDelegate;
-(void)setUserTable:(NSMutableDictionary *)userDictionary;
-(NSString *)getUsername;
-(NSString *)getFirstName;
-(NSString *)getLastName;
-(NSNumber *)getRoles;
-(NSString *)getHotelId;
@end
