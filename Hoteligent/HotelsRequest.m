//
//  Hotels.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "HotelsRequest.h"
#import "RequestHelper.h"


@implementation HotelsRequest

- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}
-(void)getHotelsIdsForToken:(NSString *)token{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper getRequestWithParameters:nil forUrl:@"api/hotels" andHeader:dic];

}
-(void)didReceiveResponse:(id)response{
    [_delegate getHotelsSuccesWithResponse:response];
    
}
-(void)didReceiveError:(NSError *)error{
    [_delegate getHotelsError:error];
    
}

@end
