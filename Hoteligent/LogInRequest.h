//
//  LogIn.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
@protocol LogInDelegate <NSObject>
-(void)loginSuccesWithResponse:(id)response;
-(void)loginError:(NSError *)error;
@end


@interface LogInRequest : NSObject{
    __weak id <LogInDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

@property(nonatomic, weak)NSString *username;
@property(nonatomic, weak)NSString *password;
-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)doLoginWithAttributes:(NSDictionary *)attributes;
@end
