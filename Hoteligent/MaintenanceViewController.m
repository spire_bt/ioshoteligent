//
//  MaintenanceViewController.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/18/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "MaintenanceViewController.h"
#import "SWRevealViewController.h"
#import "MaintenanceRequest.h"
#import "MaintenanceDetailsViewController.h"
#import "HotelsWork.h"
#import "TokenWork.h"

@interface MaintenanceViewController ()


@end

@implementation MaintenanceViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setTitle:@"Maintenance"];
    // Do any additional setup after loading the view.
    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    TokenWork *tokenWork = [[TokenWork alloc] init];
    NSString *accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];

    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        MaintenanceRequest *maintenance = [[MaintenanceRequest alloc] init];
        [maintenance setDelegate:self];
        [maintenance getMaintenance:[hotelsWork getId] withToken:accessToken];
    });

}

-(void)getMaintenanceSuccesWithResponse:(id)response{
    if ([response count]!=0) {
        _maintenanceArray = [NSMutableArray arrayWithArray:response];
        [_maintenenceTableView reloadData];

    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Empty Maintenance Response" message:@"Nothing is assigned to you, or the response can not be parsed" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        // optional - add more buttons:
        [alert show];
        
    }
}
-(void)getMaintenanceError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // optional - add more buttons:
    [alert show];
    
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_maintenanceArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *cellInfo = [NSString stringWithFormat:@"Room No. %@",[[_maintenanceArray objectAtIndex:indexPath.row]valueForKey:@"roomNumber"]];
    cell.textLabel.font = [cell.textLabel.font fontWithSize:10];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text = cellInfo;
    
    cell.detailTextLabel.font = [cell.detailTextLabel.font fontWithSize:8];
    cell.detailTextLabel.numberOfLines = 0;
    cell.detailTextLabel.lineBreakMode = NSLineBreakByWordWrapping;
    NSString *status = [NSString string];
    switch ([[[_maintenanceArray objectAtIndex:indexPath.row] valueForKey:@"status"]intValue]) {
        case 0:
            status = @"Created";
            break;
        case 1:
            status = @"Completed";
            break;
        case 2:
            status = @"Cancelled";
            break;
        default:
            status = @"";
            break;
    }

    cell.detailTextLabel.text =[NSString stringWithFormat:@"Created: %@ \nStatus: %@ \nAssigned to: %@",[[_maintenanceArray objectAtIndex:indexPath.row] valueForKey:@"created"],status,[[_maintenanceArray objectAtIndex:indexPath.row] valueForKey:@"assignedTo"]];
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.height*0.2;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    //[self performSegueWithIdentifier:@"DetailedMaintenance" sender:self];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    NSIndexPath *path = [_maintenenceTableView indexPathForSelectedRow];
    MaintenanceDetailsViewController *detailsController =[segue destinationViewController];
    detailsController.contentArray = [_maintenanceArray objectAtIndex:path.row];
    
}


@end
