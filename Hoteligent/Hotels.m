//
//  Hotels.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "Hotels.h"
#import "User.h"


@implementation Hotels

@dynamic address;
@dynamic city;
@dynamic id;
@dynamic is_selected;
@dynamic name;
@dynamic phone;
@dynamic users;
@dynamic inverse_hotels_id;

@end
