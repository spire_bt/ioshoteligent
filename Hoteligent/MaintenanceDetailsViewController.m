//
//  MaintenanceDetailsViewController.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/28/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "MaintenanceDetailsViewController.h"
#import "CustomLabel.h"
#import "ActionSheetPicker.h"
#import "MaintenanceRequest.h"
#import "HotelsWork.h"
#import "TokenWork.h"

@interface MaintenanceDetailsViewController ()

@end

@implementation MaintenanceDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];

    [_titleLabel setText:[NSString stringWithFormat:@"Maintenance - %@",[_contentArray valueForKey:@"roomNumber"]]];
    // CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = _maintenanceScrollView.frame.size;

    
    _statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 5, screenSize.width, 35)];
    
    UILabel *statusLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, 30)];
    if ([[_contentArray valueForKey:@"status"] intValue]==1) {
        [statusLabel setText:@"CREATED"];
    }else if([[_contentArray valueForKey:@"status"] intValue]==2){
        [statusLabel setText:@"COMPLETED"];
    }
    [_statusView addSubview:statusLabel];
    [_statusView setHidden:YES];
    [_maintenanceScrollView addSubview:_statusView];
    
    
    _notesView = [[UIView alloc] initWithFrame:CGRectMake(0, _statusView.frame.size.height+5, screenSize.width, 150)];
    
    UILabel *notesTitleLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 0, _notesView.frame.size.width, 30)];
    [notesTitleLabel setBackgroundColor:[UIColor grayColor]];
    [notesTitleLabel setText:@"Notes"];
    [_notesView addSubview:notesTitleLabel];
    
    self.notesTextView = [[UITextView alloc] initWithFrame:CGRectMake(0.0, notesTitleLabel.frame.size.height+5, _notesView.frame.size.width, 100)];
    
    UIEdgeInsets textViewInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0); //top, left, bottom, right
    self.notesTextView.contentInset = textViewInsets;
    self.notesTextView.scrollIndicatorInsets = textViewInsets;
    
    self.notesTextView.text = [NSString stringWithFormat:@"%@",[_contentArray valueForKey:@"notes"]];
    self.notesTextView.font = [UIFont systemFontOfSize:10.0];
    self.notesTextView.backgroundColor = [UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0];
    self.notesTextView.scrollEnabled = YES;
    self.notesTextView.alwaysBounceVertical = YES;
    self.notesTextView.editable = YES;
    self.notesTextView.clipsToBounds = YES;
    self.notesTextView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    self.notesTextView.keyboardType = UIKeyboardTypeDefault;
    self.notesTextView.textColor = [UIColor whiteColor];
    
    CALayer *imageLayer = self.notesTextView.layer;
    [imageLayer setCornerRadius:10];
    [imageLayer setBorderWidth:2];
    imageLayer.borderColor=[[UIColor grayColor] CGColor];
    [_notesView addSubview:_notesTextView];
    
    [_maintenanceScrollView addSubview:_notesView];
    
    CGFloat origin = 0.0f;
    
    _itemsView = [[UIView alloc] initWithFrame:CGRectMake(0, _notesView.frame.size.height+_notesView.frame.origin.y+5, screenSize.width, 0)];
    
    for (int i = 0; i<[[_contentArray valueForKey:@"items"] count]; i++) {
        CGFloat size = 35;
        
        UIView *categoryView = [[UIView alloc] initWithFrame:CGRectMake(0, origin, screenSize.width, size)];
        
        UIButton *inspectionTitleLabel = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, screenSize.width, 30)];
        [inspectionTitleLabel setBackgroundColor:[UIColor grayColor]];
        int itemStatus =[[[[_contentArray valueForKey:@"items"]objectAtIndex:i]valueForKey:@"itemStatus"] intValue];
        NSString *statusTitle;
        switch (itemStatus) {
            case 0:
                statusTitle = @"Out of service";
                break;
                
            case 1:
                statusTitle = @"Working";

                break;
                
            case 2:
                statusTitle = @"Ignored";

                break;
                
            default:
                statusTitle = @"";
                break;
        }
        NSString *titleForItem = [NSString stringWithFormat:@"%@ - %@",[[[[[_contentArray valueForKey:@"items"]objectAtIndex:i]valueForKey:@"name"]objectAtIndex:0] valueForKey:@"value"],statusTitle];
        
        [inspectionTitleLabel setTitle:titleForItem forState:UIControlStateNormal];
        [inspectionTitleLabel setTag:i];
        [inspectionTitleLabel addTarget:self action:@selector(openPickerView:) forControlEvents:UIControlEventTouchUpInside];
        [categoryView addSubview:inspectionTitleLabel];
        
        origin += categoryView.frame.size.height;
        [_itemsView addSubview:categoryView];
    }
    CGRect newFrame = _itemsView.frame;
    
    newFrame.size.height=origin;
    [_itemsView setFrame:newFrame];
    [_maintenanceScrollView addSubview:_itemsView];
    
   
    _completeView = [[UIView alloc] initWithFrame:CGRectMake(0, _itemsView.frame.size.height+_itemsView.frame.origin.y+5, screenSize.width, 50)];
    UIButton *completeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [completeButton addTarget:self action:@selector(complete:)
             forControlEvents:UIControlEventTouchUpInside];
    [completeButton setTitle:@"Complete" forState:UIControlStateNormal];
    completeButton.frame = CGRectMake(0, 10, screenSize.width, 40.0);
    [completeButton setTintColor:[UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0]];
    [_completeView addSubview:completeButton];
    [_maintenanceScrollView addSubview:_completeView];
    
    CGSize newSize = _maintenanceScrollView.frame.size;
    newSize.height = _completeView.frame.origin.y + _completeView.frame.size.height+10;
    [_maintenanceScrollView setContentSize:newSize];

    
}
-(void)openPickerView:(UIButton *)sender{
    int tagOfElement = [sender tag];

    NSArray *statusTitle = [NSArray arrayWithObjects:@"Out of service", @"Working", @"Ignored", nil];

    NSArray *listItems = [sender.titleLabel.text componentsSeparatedByString:@"-"];
    [ActionSheetStringPicker showPickerWithTitle:@"Select Status"
                                            rows:statusTitle
                                initialSelection:0
                                       doneBlock:^(ActionSheetStringPicker *picker, NSInteger selectedIndex, id selectedValue) {
                                           NSLog(@"Picker: %@, Index: %ld, value: %@",
                                                 picker, (long)selectedIndex, selectedValue);
                                           NSString *buttonTitle = [NSString stringWithFormat:@"%@ - %@",[listItems objectAtIndex:0],selectedValue];
                                           [sender setTitle:buttonTitle forState:UIControlStateNormal];
                                           [self selectedValue:(long)selectedIndex forButtonTag:tagOfElement];

                                       }
                                     cancelBlock:^(ActionSheetStringPicker *picker) {
                                         NSLog(@"Block Picker Canceled");
                                     }
                                          origin:sender];
    // You can also use self.view if you don't have a sender
}
-(void)selectedValue:(long)index forButtonTag:(int)tag{
    
    
    NSError *error = nil;
    
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:_contentArray
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    
    NSMutableDictionary *dictFromData = [NSJSONSerialization JSONObjectWithData:dataFromDict
                                                                        options:NSJSONReadingMutableContainers
                                                                          error:&error];

    [[[dictFromData valueForKey:@"items"] objectAtIndex:tag] setValue:[NSNumber numberWithLong:index] forKey:@"itemStatus"];
    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    TokenWork *tokenWork = [[TokenWork alloc] init];
    NSString *accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
         MaintenanceRequest *maintenance = [[MaintenanceRequest alloc] init];
        [maintenance setDelegate:self];
        [maintenance putMaintenance:dictFromData withToken:accessToken forHotelId:[hotelsWork getId] andMaintenanceActivity:[dictFromData valueForKey:@"id"]];
        
    });
    
}

-(void)complete:(id)sender{
    
    NSError *error = nil;
    
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:_contentArray
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    
    NSMutableDictionary *dictFromData = [NSJSONSerialization JSONObjectWithData:dataFromDict
                                                                        options:NSJSONReadingMutableContainers
                                                                          error:&error];
    
    [dictFromData setValue:[NSNumber numberWithInt:1] forKey:@"status"];
    [dictFromData setValue:self.notesTextView.text forKey:@"notes"];
    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    TokenWork *tokenWork = [[TokenWork alloc] init];
    NSString *accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        MaintenanceRequest *maintenance = [[MaintenanceRequest alloc] init];
        [maintenance setDelegate:self];
        [maintenance putMaintenance:dictFromData withToken:accessToken forHotelId:[hotelsWork getId] andMaintenanceActivity:[dictFromData valueForKey:@"id"]];
        
    });
    [self backSelected:self];

}
-(IBAction)backSelected:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}
-(void)getMaintenanceSuccesWithResponse:(id)response{
    _contentArray = response;

}
-(void)getMaintenanceError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // optional - add more buttons:
    [alert show];
    
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([_notesTextView isFirstResponder] && [touch view] != _notesTextView) {
        [_notesTextView resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
