//
//  Token.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "Token.h"


@implementation Token

@dynamic access_token;
@dynamic expires_in;
@dynamic for_username;
@dynamic issued;

@end
