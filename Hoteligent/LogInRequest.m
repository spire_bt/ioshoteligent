//
//  LogIn.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "LogInRequest.h"
#import "RequestHelper.h"


@implementation LogInRequest
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}
-(void)doLoginWithAttributes:(NSDictionary *)attributes{

    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper postRequestWithParameters:attributes forUrl:@"Token"];
}
-(void)didReceiveResponse:(id)response{
    [_delegate loginSuccesWithResponse:response];

}
-(void)didReceiveError:(NSError *)error{
   [_delegate loginError:error];
    
}
@end
