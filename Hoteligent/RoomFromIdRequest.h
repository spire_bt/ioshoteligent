//
//  RoomFromId.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/13/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RoomFromIdDelegate <NSObject>
-(void)getRoomByIdSuccesWithResponse:(id)response;
-(void)getRoomByIdError:(NSError *)error;
@end


@interface RoomFromIdRequest : NSObject{
    __weak id <RoomFromIdDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)getRoomByIds:(NSString *)attributes withToken:(NSString *)token;

@end
