//
//  Rooms.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/12/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol RoomsDelegate <NSObject>
-(void)getRoomsSuccesWithResponse:(id)response;
-(void)getRoomsError:(NSError *)error;
@end


@interface RoomsRequest : NSObject{
    __weak id <RoomsDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)getRoomsIdsForToken:(NSString *)token;

@end
