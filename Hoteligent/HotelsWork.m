//
//  HotelsWork.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/4/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "HotelsWork.h"

@implementation HotelsWork
//YourAppDelegate *appDelegate=( YourAppDelegate* )[UIApplication sharedApplication].delegate;

- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    _appDelegate=(AppDelegate* )[UIApplication sharedApplication].delegate;
    return self;
}
-(void)setHotelsTable:(NSMutableDictionary *)hotelsDictionary{
   // NSManagedObjectContext *context = [_appDelegate.managedObjectContext];
    Hotels *hotelsObject = [NSEntityDescription
                                       insertNewObjectForEntityForName:@"Hotels"
                                       inManagedObjectContext:_appDelegate.managedObjectContext];
    
    hotelsObject.id = [hotelsDictionary valueForKey:@"id"];
    hotelsObject.name = [hotelsDictionary valueForKey:@"name"];
   // hotelsObject.users = [hotelsDictionary valueForKey:@"users"];
    hotelsObject.is_selected = [NSNumber numberWithBool:true];
    hotelsObject.address = [hotelsDictionary valueForKey:@"address"];
    hotelsObject.city = [hotelsDictionary valueForKey:@"city"];
    hotelsObject.phone = [hotelsDictionary valueForKey:@"phone"];
   // tokenObject.inverse_hotels_id = date;
    
    
    NSError *error;
    if (![_appDelegate.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
    }
}
-(NSString *)getId{
    NSString *Id;
    
    for (Hotels *info in [self fetchObjects]) {
        Id = info.id;
    }
    return Id;
}
-(NSString *)getName{
    NSString *name;
    
    for (Hotels *info in [self fetchObjects]) {
        name = info.name;
    }
    return name;
}
/*-(NSArray *)getUsers{
    NSString *users;
    
    for (Hotels *info in [self fetchObjects]) {
        users = info.users;
    }
    NSData* data = [users dataUsingEncoding:NSUTF8StringEncoding];
    NSError *e;
    NSArray *array = [NSJSONSerialization JSONObjectWithData:data options:0 error:&e];
    return array;
}*/
-(NSString *)getAddress{
    NSString *address;
    
    for (Hotels *info in [self fetchObjects]) {
        address = info.address;
    }
    return address;
}
-(NSString *)getCity{
    NSString *city;
    
    for (Hotels *info in [self fetchObjects]) {
        city = info.city;
    }
    return city;
}
-(NSString *)getPhone{
    NSString *phone;
    
    for (Hotels *info in [self fetchObjects]) {
        phone = info.phone;
    }
    return phone;
}

-(NSArray *)fetchObjects{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Hotels" inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

@end
