//
//  User.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Hotels;

@interface User : NSManagedObject

@property (nonatomic, retain) NSString * first_name;
@property (nonatomic, retain) NSString * hotel_id;
@property (nonatomic, retain) NSNumber * id;
@property (nonatomic, retain) NSString * last_name;
@property (nonatomic, retain) id roles;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) Hotels *hotels_id;

@end
