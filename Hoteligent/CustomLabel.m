//
//  CustomLabel.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/30/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "CustomLabel.h"

@implementation CustomLabel


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self) {
        self.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 0);
    }
    return self;
}

- (void)drawRect:(CGRect)rect {

    [self setFont:[UIFont systemFontOfSize:10]];
    self.numberOfLines = 1;
    //self.baselineAdjustment = UIBaselineAdjustmentAlignBaselines; // or UIBaselineAdjustmentAlignCenters, or UIBaselineAdjustmentNone
    self.clipsToBounds = YES;
    self.backgroundColor = [UIColor clearColor];
    self.textColor = [UIColor blackColor];
    self.textAlignment = NSTextAlignmentLeft;
    [super drawTextInRect:UIEdgeInsetsInsetRect(rect, self.edgeInsets)];

}


@end
