//
//  RoomFromId.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/13/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "RoomFromIdRequest.h"
#import "RequestHelper.h"


@implementation RoomFromIdRequest
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    return self;
}


-(void)getRoomByIds:(NSString *)attributes withToken:(NSString *)token{
    NSDictionary *dic = [NSDictionary dictionaryWithObject:token forKey:@"Authorization"];
    NSString *urlString = [NSString stringWithFormat:@"api/rooms/%@",attributes];
    RequestHelper *helper = [[RequestHelper alloc] init];
    [helper setDelegate:self];
    [helper getRequestWithParameters:nil forUrl:urlString andHeader:dic];
    
}
-(void)didReceiveResponse:(id)response{
    [_delegate getRoomByIdSuccesWithResponse:response];
    
}
-(void)didReceiveError:(NSError *)error{
    [_delegate getRoomByIdError:error];
    
}

@end
