//
//  NotificationViewController.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/18/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "NotificationViewController.h"
#import "SWRevealViewController.h"
#import "NotificationsRequest.h"
#import "NotificationDetailsViewController.h"


@interface NotificationViewController ()

@end

@implementation NotificationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}


- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self setTitle:@"Notifications"];
    _barButton.target = self.revealViewController;
    _barButton.action = @selector(revealToggle:);
    
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
   NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        NotificationsRequest *notifications = [[NotificationsRequest alloc] init];
        [notifications setDelegate:self];
        [notifications getNotifications:[[defaults objectForKey:@"userInfo"]objectForKey:@"hotelId"] withToken:[[defaults objectForKey:@"userInfo"]objectForKey:@"accessToken"]];
        
    });
}


-(void)getNotificationsSuccesWithResponse:(id)response{
    NSLog(@"response %@",response);
    _notificationArray = [NSMutableArray arrayWithArray:response];
    [_notificationTableView reloadData];
   
}
-(void)getNotificationsError:(NSError *)error{
    /*TODO: Error Alert*/
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return [_notificationArray count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier forIndexPath:indexPath];
    
    // Configure the cell...
    NSString *cellInfo = [NSString stringWithFormat:@"%@",[[[[_notificationArray objectAtIndex:indexPath.row] valueForKey:@"message"] objectAtIndex:0] valueForKey:@"value"]];
    cell.textLabel.font = [cell.textLabel.font fontWithSize:10];
    cell.textLabel.numberOfLines = 0;
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.text = cellInfo;
    
    
    return cell;
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    return screenRect.size.height*0.2;
 }
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    [self performSegueWithIdentifier:@"DetailedNotification" sender:self];

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}


@end
