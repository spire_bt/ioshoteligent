//
//  HousekeepengDetailsViewController.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/28/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "HousekeepengDetailsViewController.h"
#import "CustomLabel.h"
#import "HousekeepingRequest.h"
#import "HotelsWork.h"
#import "TokenWork.h"
#import "UsersWork.h"

@interface HousekeepengDetailsViewController (){
}

@end

@implementation HousekeepengDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [_titleLabel setText:[NSString stringWithFormat:@"Housekeepeng - %@",[_contentArray valueForKey:@"roomNumber"]]];
   // CGRect screenBound = [[UIScreen mainScreen] bounds];
    _screenSize = _housekeepengScrollView.frame.size;
    _pausedTimeInSeconds = 0;
    _currentTimeInSeconds = 0;
    if([_contentArray valueForKey:@"pausedTime"]!=[NSNull null]){
        NSTimeInterval interval = [[_contentArray valueForKey:@"pausedTime"] doubleValue];
        _pausedTimeInSeconds = interval;
    }
// NSLog(@"_contentArray %@",_contentArray);
    _timerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _screenSize.width, 40)];

    NSString * createdText = [NSString stringWithFormat:@"Created %@",[[_contentArray valueForKey:@"created" ] substringWithRange:NSMakeRange(0, 10)]];
    _createdLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 0, [self returnSizeForText:createdText].width+15, [self returnSizeForText:createdText].height)];
    _createdLabel.text = createdText;
    [_timerView addSubview:_createdLabel];

    NSString * assignedToText = [NSString stringWithFormat:@"%@ %@",[_contentArray valueForKey:@"firstName" ],[_contentArray valueForKey:@"lastName" ]];
    UILabel *assignedToLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(0, _createdLabel.frame.size.height+5, [self returnSizeForText:assignedToText].width+15, [self returnSizeForText:assignedToText].height)];
    assignedToLabel.text = assignedToText;
    [_timerView addSubview:assignedToLabel];

    _startPauseButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [_startPauseButton addTarget:self action:@selector(startPause:)
     forControlEvents:UIControlEventTouchUpInside];
    [_startPauseButton setTitle:@"Start" forState:UIControlStateNormal];
    _startPauseButton.frame = CGRectMake(_screenSize.width - 100, 0, 100.0, 40.0);
    [_startPauseButton setTintColor:[UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0]];
    [_startPauseButton setTag:3];
    [_timerView addSubview:_startPauseButton];
    
    [_housekeepengScrollView addSubview:_timerView];
    
    [self inspectionViewUI];
    [self notesViewUI];
    [self categoriesViewUI];
    [self compeleteViewUI];
    
    
    CGSize newSize = _housekeepengScrollView.frame.size;
    newSize.height = _completeView.frame.origin.y + _completeView.frame.size.height+10;
    [_housekeepengScrollView setContentSize:newSize];
    if ([_contentArray valueForKey:@"status"]==[NSNumber numberWithInt:4]||[_contentArray valueForKey:@"status"]==[NSNumber numberWithInt:3]) {
        [_startPauseButton setUserInteractionEnabled:NO];
        [_allCategoriesView setUserInteractionEnabled:NO];
        [_completeView setUserInteractionEnabled:NO];
    }else{
        [_startPauseButton setUserInteractionEnabled:YES];
        [_allCategoriesView setUserInteractionEnabled:YES];
        [_completeView setUserInteractionEnabled:YES];

    }

}
-(void)inspectionViewUI{
    
    _inspectionView = [[UIView alloc] initWithFrame:CGRectMake(0, _timerView.frame.size.height+5, _screenSize.width, 120)];
    UILabel *inspectionTitleLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 0, _inspectionView.frame.size.width, 30)];
    [inspectionTitleLabel setBackgroundColor:[UIColor grayColor]];
    [inspectionTitleLabel setText:@"Inspection"];
    [_inspectionView addSubview:inspectionTitleLabel];
    NSString *status = [NSString string];
    switch ([[_contentArray valueForKey:@"inspectionStatus"]intValue]) {
        case Pending:
            status = @"Pending";
            break;
        case Passed:
            status = @"Passed";
            break;
        case Failed:
            status = @"Failed";
            break;
        default:
            status = @"";
            break;
    }

    NSString *inspectionStatusString = [NSString stringWithFormat:@"Inspection Status \n%@",status];
    
    UILabel *inspectionStatus = [[UILabel alloc]initWithFrame:CGRectMake(0, inspectionTitleLabel.frame.size.height+5, _inspectionView.frame.size.width, 30)];
    [inspectionStatus setBackgroundColor:[UIColor clearColor]];
    [inspectionStatus setText:inspectionStatusString];
    [inspectionStatus setNumberOfLines:0];
    [inspectionStatus setTextColor:[UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0]];
    [inspectionStatus setFont:[UIFont systemFontOfSize:10]];
    [_inspectionView addSubview:inspectionStatus];
    
    
    UIButton *passInspectionButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [passInspectionButton addTarget:self action:@selector(inspectionActivity:)
                forControlEvents:UIControlEventTouchUpInside];
    [passInspectionButton setTitle:@"Pass" forState:UIControlStateNormal];
    passInspectionButton.frame = CGRectMake(0, inspectionStatus.frame.size.height+inspectionStatus.frame.origin.y+5, (_screenSize.width/2), 60.0);
    [passInspectionButton setTintColor:[UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0]];
    [passInspectionButton setTag:passButtonTag];
    [_inspectionView addSubview:passInspectionButton];
    
    UIButton *failInspectionButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [failInspectionButton addTarget:self action:@selector(inspectionActivity:)
                   forControlEvents:UIControlEventTouchUpInside];
    [failInspectionButton setTitle:@"Fail" forState:UIControlStateNormal];
    failInspectionButton.frame = CGRectMake(_screenSize.width/2, inspectionStatus.frame.size.height+inspectionStatus.frame.origin.y+5, (_screenSize.width/2), 60.0);
    [failInspectionButton setTintColor:[UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0]];
    [failInspectionButton setTag:failButtonTag];
    [_inspectionView addSubview:failInspectionButton];

    
    [_inspectionView setHidden:YES];
    if ([_contentArray valueForKey:@"status"]==[NSNumber numberWithInt:4]||[_contentArray valueForKey:@"status"]==[NSNumber numberWithInt:3]) {
        [_inspectionView setHidden:NO];
    }
    [_housekeepengScrollView addSubview:_inspectionView];
    

}
-(void)notesViewUI{
    if ([_inspectionView isHidden]) {
        _notesOrigin = _inspectionView.frame.origin.y;
    }else{
        _notesOrigin = _inspectionView.frame.size.height+5+_inspectionView.frame.origin.y;
        
    }
    _notesView = [[UIView alloc] initWithFrame:CGRectMake(0, _notesOrigin, _screenSize.width, 150)];
    
    UILabel *notesTitleLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 0, _notesView.frame.size.width, 30)];
    [notesTitleLabel setBackgroundColor:[UIColor grayColor]];
    [notesTitleLabel setText:@"Notes"];
    [_notesView addSubview:notesTitleLabel];
    
    self.notesTextView = [[UITextView alloc] initWithFrame:CGRectMake(0.0, notesTitleLabel.frame.size.height+5, _notesView.frame.size.width, 100)];
    
    UIEdgeInsets textViewInsets = UIEdgeInsetsMake(0.0, 0.0, 0.0, 0.0); //top, left, bottom, right
    self.notesTextView.contentInset = textViewInsets;
    self.notesTextView.scrollIndicatorInsets = textViewInsets;
    
    self.notesTextView.text = [NSString stringWithFormat:@"%@",[_contentArray valueForKey:@"notes"]];
    self.notesTextView.font = [UIFont systemFontOfSize:10.0];
    self.notesTextView.backgroundColor = [UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0];
    self.notesTextView.scrollEnabled = YES;
    self.notesTextView.alwaysBounceVertical = YES;
    self.notesTextView.editable = YES;
    self.notesTextView.clipsToBounds = YES;
    self.notesTextView.keyboardDismissMode = UIScrollViewKeyboardDismissModeInteractive;
    self.notesTextView.keyboardType = UIKeyboardTypeDefault;
    self.notesTextView.textColor = [UIColor whiteColor];
    
    CALayer *imageLayer = self.notesTextView.layer;
    [imageLayer setCornerRadius:10];
    [imageLayer setBorderWidth:2];
    imageLayer.borderColor=[[UIColor grayColor] CGColor];
    [_notesView addSubview:_notesTextView];
    
    [_housekeepengScrollView addSubview:_notesView];
}
-(void)categoriesViewUI{
    CGFloat origin = 0.0f;
    
    _allCategoriesView = [[UIView alloc] initWithFrame:CGRectMake(0, _notesView.frame.size.height+_notesOrigin+5, _screenSize.width, 0)];
    
    for (int i = 0; i<[[_contentArray valueForKey:@"categories"] count]; i++) {
        CGFloat size = [[[[_contentArray valueForKey:@"categories"]objectAtIndex:i]valueForKey:@"items"]count]*35+30;
        
        UIView *categoryView = [[UIView alloc] initWithFrame:CGRectMake(0, origin, _screenSize.width, size)];
        
        UILabel *inspectionTitleLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(0, 0, _screenSize.width, 30)];
        [inspectionTitleLabel setBackgroundColor:[UIColor grayColor]];
        [inspectionTitleLabel setText:[[[[[_contentArray valueForKey:@"categories"]objectAtIndex:i]valueForKey:@"name"]objectAtIndex:0] valueForKey:@"value"]];
        [categoryView addSubview:inspectionTitleLabel];
        for (int j = 0; j<[[[[_contentArray valueForKey:@"categories"]objectAtIndex:i]valueForKey:@"items"]count]; j++) {
            int switchTag = [[NSString stringWithFormat:@"%d%d",i,j] intValue];
            UISwitch *switchElement = [[UISwitch alloc] initWithFrame:CGRectMake(0, 30+35*j, 0, 0)];
            [switchElement setTag:switchTag];
            [switchElement addTarget:self action:@selector(changeSwitch:) forControlEvents:UIControlEventValueChanged];

            if ([[[[[[_contentArray valueForKey:@"categories"]objectAtIndex:i]valueForKey:@"items"]objectAtIndex:j] valueForKey:@"value"] isEqualToString:@"on"]) {
                [switchElement setOn:YES];
            }
            [categoryView addSubview:switchElement];
            
            UILabel *descriptionLabel = [[CustomLabel alloc]initWithFrame:CGRectMake(switchElement.frame.size.width, 30+35*j, _screenSize.width -switchElement.frame.size.width, 35)];
            [descriptionLabel setText:[[[[[[[_contentArray valueForKey:@"categories"]objectAtIndex:i]valueForKey:@"items"]objectAtIndex:j] valueForKey:@"name"] objectAtIndex:0] valueForKey:@"value"]];
            [categoryView addSubview:descriptionLabel];
            
        }
        
        origin += categoryView.frame.size.height;
        [_allCategoriesView addSubview:categoryView];
    }
    CGRect newFrame = _allCategoriesView.frame;
    
    newFrame.size.height=origin;
    [_allCategoriesView setFrame:newFrame];
    [_housekeepengScrollView addSubview:_allCategoriesView];
    
}
-(void)compeleteViewUI{
    _completeView = [[UIView alloc] initWithFrame:CGRectMake(0, _allCategoriesView.frame.size.height+_allCategoriesView.frame.origin.y+5, _screenSize.width, 50)];
    UIButton *completeButton = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [completeButton addTarget:self action:@selector(complete:)
             forControlEvents:UIControlEventTouchUpInside];
    [completeButton setTitle:@"Complete" forState:UIControlStateNormal];
    completeButton.frame = CGRectMake(0, 10, _screenSize.width, 40.0);
    [completeButton setTintColor:[UIColor colorWithRed:0.96 green:0.44 blue:0.45 alpha:1.0]];
    [_completeView addSubview:completeButton];
    [_housekeepengScrollView addSubview:_completeView];
    
}
-(CGSize)returnSizeForText:(NSString *)text{
    
    CGRect screenBound = [[UIScreen mainScreen] bounds];
    CGSize screenSize = screenBound.size;
    CGFloat screenWidth = screenSize.width;
    
    UIFont *font = [UIFont systemFontOfSize:10];
    NSAttributedString *attributedText =
    [[NSAttributedString alloc] initWithString:text
                                    attributes:@{NSFontAttributeName: font}];
    CGRect rect = [attributedText boundingRectWithSize:(CGSize){screenWidth, 40}
                                               options:NSStringDrawingUsesLineFragmentOrigin
                                               context:nil];
    CGSize size = rect.size;

    return size;
}
-(void)startPause:(id)sender{
    if ([sender tag]==1) {
        //start selected
        [_startPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        _myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabelForTimerValue:) userInfo:nil repeats:YES];
        [_pauseTimer invalidate];
        [_startPauseButton setTag:2];
    }else if ([sender tag]==2) {
        //pause selected
        [_startPauseButton setTitle:@"Start" forState:UIControlStateNormal];
        [_myTimer invalidate];
        _pauseTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updatePauseValue:) userInfo:nil repeats:YES];
        [_startPauseButton setTag:1];
    } if ([sender tag]==3) {
        //start selected first time
        _startDate = [NSDate date];
        [_startPauseButton setTitle:@"Pause" forState:UIControlStateNormal];
        _myTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateLabelForTimerValue:) userInfo:nil repeats:YES];
        [_pauseTimer invalidate];
        [_startPauseButton setTag:2];
    }


    
}
-(void)updatePauseValue:(NSTimer *)timer{
    _pausedTimeInSeconds++;
    _pauseValue = [NSString stringWithFormat:@"%@",[self formattedTime:_pausedTimeInSeconds]];
}
-(void)updateLabelForTimerValue:(NSTimer *)timer {
    
    _currentTimeInSeconds++;
    NSString * createdText = [NSString stringWithFormat:@"Started %@",[self formattedTime:_currentTimeInSeconds]];
    _createdLabel.text = createdText;

}
- (NSString *)formattedTime:(int)totalSeconds
{
    
    int seconds = totalSeconds % 60;
    int minutes = (totalSeconds / 60) % 60;
    int hours = totalSeconds / 3600;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d",hours, minutes, seconds];
}
-(void)changeSwitch:(UISwitch *)switchElement{
    int tagOfElement = [switchElement tag];
    NSString *string;
    if (tagOfElement<10) {
        string = [NSString stringWithFormat: @"0%i", tagOfElement];
    }else{
        string = [NSString stringWithFormat: @"%i", tagOfElement];
    }
    NSMutableArray *tagElements = [self toCharArray:string];
    
    NSError *error = nil;

    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:_contentArray
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    
    NSMutableDictionary *dictFromData = [NSJSONSerialization JSONObjectWithData:dataFromDict
                                                                 options:NSJSONReadingMutableContainers
                                                                   error:&error];
    
    
    
    if([switchElement isOn]){
        
        [[[[[dictFromData valueForKey:@"categories"]objectAtIndex:[[tagElements objectAtIndex:0] intValue]]valueForKey:@"items"]objectAtIndex:[[tagElements objectAtIndex:1] intValue]] setValue:@"on" forKey:@"value"];

    } else{
        [[[[[dictFromData valueForKey:@"categories"]objectAtIndex:[[tagElements objectAtIndex:0] intValue]]valueForKey:@"items"]objectAtIndex:[[tagElements objectAtIndex:1] intValue]] setValue:@"" forKey:@"value"];
    }
    [dictFromData setValue:[self getStringFromDate:_startDate] forKey:@"started"];
    NSTimeInterval interval;
    if(_pausedTimeInSeconds!=0){
        interval = _pausedTimeInSeconds;
    }

    [dictFromData setValue:[NSNumber numberWithDouble:interval] forKey:@"pausedTime"];
    
    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    TokenWork *tokenWork = [[TokenWork alloc] init];
    NSString *accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        HousekeepingRequest *housekeepeng = [[HousekeepingRequest alloc] init];
        [housekeepeng setDelegate:self];
        [housekeepeng putHousekeepeng:dictFromData withToken:accessToken forHotelId:[hotelsWork getId] andHousekeepingActivity:[dictFromData valueForKey:@"id"]];
        
    });

}
- (NSMutableArray *) toCharArray : (NSString *) str
{
    
    NSMutableArray *characters = [[NSMutableArray alloc] initWithCapacity:[str length]];
    for (int i=0; i < [str length]; i++)
    {
        NSString *ichar  = [NSString stringWithFormat:@"%c", [str characterAtIndex:i]];
        [characters addObject:ichar];
    }
    
    return characters;
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    
    UITouch *touch = [[event allTouches] anyObject];
    if ([_notesTextView isFirstResponder] && [touch view] != _notesTextView) {
        [_notesTextView resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}
-(void)complete:(id)sender{
    NSError *error = nil;
    
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:_contentArray
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    
    NSMutableDictionary *dictFromData = [NSJSONSerialization JSONObjectWithData:dataFromDict
                                                                        options:NSJSONReadingMutableContainers
                                                                          error:&error];
    
    NSDate *now = [NSDate date];
    [dictFromData setValue:_notesTextView.text forKey:@"notes"];
    [dictFromData setValue:_pauseValue forKey:@"pausedTime"];
    [dictFromData setValue:@"3" forKey:@"status"];
    [dictFromData setValue:[self getStringFromDate:_startDate] forKey:@"started"];
    [dictFromData setValue:[self getStringFromDate:now] forKey:@"completed"];
    NSTimeInterval interval;
    if(_pausedTimeInSeconds!=0){
        interval = _pausedTimeInSeconds;
    }
    
    [dictFromData setValue:[NSNumber numberWithDouble:interval] forKey:@"pausedTime"];

    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    TokenWork *tokenWork = [[TokenWork alloc] init];
    NSString *accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        HousekeepingRequest *housekeepeng = [[HousekeepingRequest alloc] init];
        [housekeepeng setDelegate:self];
        [housekeepeng putHousekeepeng:dictFromData withToken:accessToken forHotelId:[hotelsWork getId] andHousekeepingActivity:[dictFromData valueForKey:@"id"]];
        
    });
    [self backSelected:self];

}
-(void)inspectionActivity:(UIButton *)sender{
    int tagOfElement = [sender tag];
    NSError *error = nil;
    
    NSData *dataFromDict = [NSJSONSerialization dataWithJSONObject:_contentArray
                                                           options:NSJSONReadingAllowFragments
                                                             error:&error];
    
    NSMutableDictionary *dictFromData = [NSJSONSerialization JSONObjectWithData:dataFromDict
                                                                        options:NSJSONReadingMutableContainers
                                                                          error:&error];
    
    
    
    HotelsWork *hotelsWork = [[HotelsWork alloc] init];
    TokenWork *tokenWork = [[TokenWork alloc] init];
    UsersWork *usersWork = [[UsersWork alloc] init];
    NSDate *now = [NSDate date];
    
    [dictFromData setValue:[usersWork getUsername] forKey:@"inspectedBy"];
    [dictFromData setValue:_notesTextView.text forKey:@"inspectionNotes"];
    [dictFromData setValue:@"4" forKey:@"status"];
    [dictFromData setValue:[NSNumber numberWithInt:tagOfElement] forKey:@"inspectionStatus"];
    [dictFromData setValue:[self getStringFromDate:now] forKey:@"inspected"];
    [dictFromData setValue:[self getStringFromDate:_startDate] forKey:@"started"];
    NSTimeInterval interval;
    if(_pausedTimeInSeconds!=0){
        interval = _pausedTimeInSeconds;
    }
    
    [dictFromData setValue:[NSNumber numberWithDouble:interval] forKey:@"pausedTime"];


    NSString *accessToken = [NSString stringWithFormat:@"Bearer %@",[tokenWork getToken]];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    
    dispatch_async(queue, ^{
        HousekeepingRequest *housekeepeng = [[HousekeepingRequest alloc] init];
        [housekeepeng setDelegate:self];
        [housekeepeng putHousekeepeng:dictFromData withToken:accessToken forHotelId:[hotelsWork getId] andHousekeepingActivity:[dictFromData valueForKey:@"id"]];
        
    });
    [self backSelected:self];

}
-(NSString *)getStringFromDate:(NSDate *)date{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd'T'HH:mm:ssZZZ"];
    NSString *stringFromDate = [formatter stringFromDate:date];

    return stringFromDate;
}
-(void)getHousekeepengSuccesWithResponse:(id)response{
    _contentArray = response;
}

-(void)getHousekeepengError:(NSError *)error{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[error localizedDescription] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
    // optional - add more buttons:
    [alert show];
    
}

-(IBAction)backSelected:(id)sender{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
