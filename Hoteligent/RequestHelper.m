//
//  RequestHelper.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/6/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "RequestHelper.h"
#import <AFNetworking.h>

NSString * const kBaseUrl = @"http://hoteligentdev.azurewebsites.net/";


@implementation RequestHelper
- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    return self;
}
-(void)postRequestWithParameters:(NSDictionary *)parameters forUrl:(NSString *)url{
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,url];

    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    //parameters = @{@"foo": @"bar"};
    [manager POST:fullUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self returnResponse:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self returnError:error];
    }];
}
-(void)getRequestWithParameters:(NSDictionary *)parameters forUrl:(NSString *)url andHeader:(NSDictionary *)header{
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,url];
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    if (header!=nil && header.count>0) {
       AFHTTPRequestSerializer *requestSerializer = [AFHTTPRequestSerializer serializer];
        for (NSString* key in header) {
            [requestSerializer setValue:[header valueForKey:key] forHTTPHeaderField:key];
        }
        manager.requestSerializer = requestSerializer;
    }
    [manager GET:fullUrl parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self returnResponse:responseObject];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self returnError:error];
    }];

}
-(void)putRequestWithParameters:(NSMutableDictionary *)jsonDictionary forUrl:(NSString *)url andHeader:(NSDictionary *)header{
    NSString *fullUrl = [NSString stringWithFormat:@"%@%@",kBaseUrl,url];

    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonDictionary options:0 error:&error];

    
    
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:[fullUrl stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]]
                                                           cachePolicy:NSURLRequestReloadIgnoringCacheData  timeoutInterval:240];
    
    [request setHTTPMethod:@"PUT"];
    NSDictionary* newRequestHTTPHeader = [[NSMutableDictionary alloc] init];

    if (header!=nil && header.count>0) {
        for (NSString* key in header) {
            [newRequestHTTPHeader setValue:[header valueForKey:key] forKey:key];
           // [request setValue:@"Basic: someValue" forHTTPHeaderField:@"Authorization"];
        }
    }
    [newRequestHTTPHeader setValue:@"application/json" forKey:@"Content-Type"];
    
    [request setAllHTTPHeaderFields:newRequestHTTPHeader];
    [request setHTTPBody:jsonData];
    
    

    AFHTTPRequestOperation *op = [[AFHTTPRequestOperation alloc] initWithRequest:request];
    op.responseSerializer = [AFJSONResponseSerializer serializer];
    [op setCompletionBlockWithSuccess:^(AFHTTPRequestOperation *operation, id responseObject) {
        [self returnResponse:responseObject];

        NSLog(@"JSON responseObject: %@ ",responseObject);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self returnError:error];

    }];
    [op start];
}
-(void)returnResponse:(id)response{
   // NSLog(@"JSON: %@", response);
    [_delegate didReceiveResponse:response];
    
}
-(void)returnError:(NSError *)error{
    NSLog(@"Error: %@", error);
    [_delegate didReceiveError:error];

}

@end
