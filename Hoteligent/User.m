//
//  User.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/7/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "User.h"
#import "Hotels.h"


@implementation User

@dynamic first_name;
@dynamic hotel_id;
@dynamic id;
@dynamic last_name;
@dynamic roles;
@dynamic username;
@dynamic hotels_id;

@end
