//
//  ViewController.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/5/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIButton *Login;
@property (weak, nonatomic) IBOutlet UITextField *username;
@property (weak, nonatomic) IBOutlet UITextField *password;
@property (weak, nonatomic) IBOutlet UIButton *hideKeyboardButton;
@property (nonatomic, strong) NSString *accessToken;
@property (nonatomic, strong) NSString *usernameString;
@property (nonatomic, strong) NSString *hotelId;
@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *hotelName;
@property (nonatomic, strong) NSString *hotelPhone;
@property (nonatomic, strong) NSString *hotelCity;
@property (nonatomic, strong) NSString *hotelAddress;
@property (nonatomic, strong) NSArray *userRole;
@property(nonatomic, strong) AppDelegate *appDelegate;

@end

