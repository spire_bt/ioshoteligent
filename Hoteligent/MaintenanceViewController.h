//
//  MaintenanceViewController.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/18/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MaintenanceViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *barButton;
@property (weak, nonatomic) IBOutlet UITableView *maintenenceTableView;
@property (strong, nonatomic) NSMutableArray *maintenanceArray;


@end
