//
//  Maintenance.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 9/13/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol MaintenanceDelegate <NSObject>
-(void)getMaintenanceSuccesWithResponse:(id)response;
-(void)getMaintenanceError:(NSError *)error;
@end


@interface MaintenanceRequest : NSObject{
    __weak id <MaintenanceDelegate> _delegate;
}
@property (nonatomic, weak)  id delegate;

-(void)didReceiveResponse:(id)response;
-(void)didReceiveError:(NSError *)error;
-(void)getMaintenance:(NSString *)attributes withToken:(NSString *)token;
-(void)putMaintenance:(NSMutableDictionary *)jsonDictionary withToken:(NSString *)token forHotelId:(NSString*)hotelId andMaintenanceActivity:(NSString *)activityId;

@end
