//
//  UsersWork.m
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/4/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import "UsersWork.h"

@implementation UsersWork

- (id)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    _appDelegate=(AppDelegate* )[UIApplication sharedApplication].delegate;
    return self;
}
-(void)setUserTable:(NSMutableDictionary *)userDictionary{
    
    
    User *userObject = [NSEntityDescription
                          insertNewObjectForEntityForName:@"User"
                          inManagedObjectContext:_appDelegate.managedObjectContext];
    
    userObject.username = [userDictionary valueForKey:@"userName"];
    userObject.first_name = [userDictionary valueForKey:@"firstName"];
    userObject.last_name = [userDictionary valueForKey:@"lastName"];
    userObject.roles = [userDictionary valueForKey:@"roles"];;
    userObject.hotel_id = [userDictionary valueForKey:@"hotelId"];
    //userObject.hotels_id = [userDictionary valueForKey:@"hotelId"];

    
    NSError *error;
    if (![_appDelegate.managedObjectContext save:&error]) {
        NSLog(@"Whoops, couldn't save: %@", [error localizedDescription]);
        
    }else{
        
        
    }
}
-(NSString *)getUsername{
    NSString *usernameString;
    
    for (User *info in [self fetchObjects]) {
        usernameString = info.username;
    }
    return usernameString;
    
}
-(NSString *)getFirstName{
    NSString *first_nameString;
    
    for (User *info in [self fetchObjects]) {
        first_nameString = info.first_name;
    }
    return first_nameString;
    
}
-(NSString *)getLastName{
    NSString *last_nameString;
    
    for (User *info in [self fetchObjects]) {
        last_nameString = info.last_name;
    }
    return last_nameString;
    
}
-(NSArray *)getRoles{
    NSArray *rolesNumber;
    
    for (User *info in [self fetchObjects]) {
        rolesNumber = info.roles;
    }
    return rolesNumber;
    
}
-(NSString *)getHotelId{
    NSString *hotel_idString;
    
    for (User *info in [self fetchObjects]) {
        hotel_idString = info.hotel_id;
    }
    return hotel_idString;
    
}

-(NSArray *)fetchObjects{
    NSError *error;
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"User" inManagedObjectContext:_appDelegate.managedObjectContext];
    [fetchRequest setEntity:entity];
    NSArray *fetchedObjects = [_appDelegate.managedObjectContext executeFetchRequest:fetchRequest error:&error];
    
    return fetchedObjects;
}

@end
