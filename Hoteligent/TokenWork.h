//
//  TokenWork.h
//  Hoteligent
//
//  Created by Spire Jankulovski on 10/4/15.
//  Copyright (c) 2015 Spire Jankulovski. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppDelegate.h"
#import "Token.h"

@interface TokenWork : NSObject
@property(nonatomic, strong) AppDelegate *appDelegate;
-(void)setTokenTable:(NSMutableDictionary *)tokenDictionary;
-(NSString *)getToken;
-(NSDate *)getIssued;
-(NSString *)getUsernameForToken;
-(NSNumber *)getExpiresIn;
-(NSArray *)fetchObjects;
@end
